package com.example.firebaseapp;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

// It extends from RecyclerView.Adapter because we want to take some date and put it on the list container (Recycler View)
public class ListItemAdapter extends RecyclerView.Adapter<ListItemAdapter.ListItemViewHolder>
{
    // List of To Do elements that we will display in the RecyclerView
    List<ToDo> todoList;

    //Basic constructor of the class
    public ListItemAdapter(MainActivity main, List<ToDo> todoList) {

        this.todoList = todoList;
    }

    @NonNull
    @Override
    public ListItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);

        return new ListItemViewHolder(view);
    }

    //Called by RecyclerView to display the data at a specified position.
    // This method should update the contents of the itemView to reflect the item at the given position.
    //This method will be called for every element of the list in order to display it
    @Override
    public void onBindViewHolder(@NonNull ListItemViewHolder listItemViewHolder, int i) {

        //First, we obtain an element of the list in a specific i position
        ToDo element = todoList.get(i);

        //Now, we set the corresponding values to the itemView (the specific view for a particular item)
        //This specific view is provided automatically when this method is executed
        listItemViewHolder.item_task.setText(element.getTask());
        listItemViewHolder.item_description.setText(element.getDescription());
        listItemViewHolder.item_date.setText(element.getDate());

    }

    @Override
    public int getItemCount() {
        return todoList.size();
    }

    //This nested class represents an specific item of the To Do list.
    //We need it to have control over the details of the list items
    public class ListItemViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener
    {
        //Attributes of an item

        public TextView item_task;
        public TextView item_description;
        public TextView item_date;

        //Public constructor
        public ListItemViewHolder(@NonNull View itemView) {
            super(itemView);


            itemView.setOnCreateContextMenuListener(this);

            //We initialize every value considering the view components defined in list-item.xml

            item_task = (TextView) itemView.findViewById(R.id.item_task);
            item_description = (TextView) itemView.findViewById(R.id.item_description);
            item_date = (TextView) itemView.findViewById(R.id.item_date);

        }


        //With this method we create a context menu that is display every time we make a long time click on any To Do item.
        //This menu will provide the option to delete the item we want.
        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select the action");
            menu.add(0,0,getAdapterPosition(),"DELETE");

        }
    }
}
