package com.example.firebaseapp;

import java.io.Serializable;

public class ToDo implements Serializable {

    private String id, task, description;

    private String date;

    public ToDo(){

    }

    //We don't create it with id, because the database will do this for us
    public ToDo(String task, String description, String date) {

        this.task = task;
        this.description = description;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
