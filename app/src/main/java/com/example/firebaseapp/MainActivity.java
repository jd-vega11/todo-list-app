package com.example.firebaseapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    // Your To Do list
    private List<ToDo> toDoList = new ArrayList<>();

    //The Firestore db representation
    private FirebaseFirestore db;

    //The To Do list container an its layout manager
    private RecyclerView toDoListContainer;
    private RecyclerView.LayoutManager layoutManager;

    //The add button to create more To Do items
    private Button button;

    //The item components
    public TextView task, description, date;

    //The adapter to fill in the list with the added information
    private ListItemAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //Get an instance of your database
        db = FirebaseFirestore.getInstance();

        //Initialize the item components considering the views defined in the activity_main.xml
        task = (TextView) findViewById(R.id.task);
        description = (TextView) findViewById(R.id.description);
        date = (TextView) findViewById(R.id.date);

        //Initialize the add button
        button = (Button) findViewById(R.id.addButton);

        //Initialize the RecyclerView that will contain the To Do list
        toDoListContainer = (RecyclerView) findViewById(R.id.listTodo);
        toDoListContainer.setHasFixedSize(true);

        //Define a layout manager for the RecyclerView
        layoutManager = new LinearLayoutManager(this);
        toDoListContainer.setLayoutManager(layoutManager);

        //Initilize the adapter that we talked about earlier
        adapter = new ListItemAdapter(MainActivity.this, toDoList);
        toDoListContainer.setAdapter(adapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addData(task.getText().toString(), description.getText().toString(), date.getText().toString());

            }
        });

        loadData();

    }


    @Override
    public boolean onContextItemSelected(MenuItem item){
        if(item.getTitle().equals("DELETE"))
        {
            deleteItem(item.getOrder());
        }

        return super.onContextItemSelected(item);

    }

    private void deleteItem(int order)
    {
        //Call the collection that contains the ToDoList information (all of its items)
        db.collection("ToDoList")
                //obtain the id of the item that you want to delete based on its position within the complete list.
                .document(toDoList.get(order).getId())
                //Invoke the delete method over that item.
                .delete()
                //Add a listener that will know when the process is finished
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //Reload the data to update the view
                        loadData();
                        Log.d("TODOList-delete", "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //Print error messages in case of failure.
                        Log.w("TODOList-delete", "Error deleting document", e);
                    }
                });
    }

    private void addData(String task, String description, String date)
    {
        //Define the new item that you want to add to the database
        ToDo item = new ToDo(task, description, date);

        //Call the collection that contains the ToDoList information (all of its items)
        db.collection("ToDoList")
                //Invoke the add method that receives the new item as parameter
                .add(item)
                //Add a listener that will know when the process is finished
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {

                        //In case of success, call the loadData() method to update the list with the new database item.
                        loadData();
                        Log.d("TODOList-add", "DocumentSnapshot written with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //Print error messages in case of failure.
                        Log.w("TODOList-add", "Error adding document", e);
                    }
                });

    }

    private void loadData() {

        //Since we query all collection items, we need to clear our local list to avoid repeated items
        if(toDoList.size() > 0){
            toDoList.clear();
        }

        //Call the collection that contains the ToDoList information (all of its items)
        db.collection("ToDoList")
                //Invoke the get method to obtain all the elements in the collection.
                .get()
                //Add a listener that will know when the process is finished
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        //Ask if the task was successful
                        if (task.isSuccessful()) {

                            //Iterate over all the elements of the collection
                            for (QueryDocumentSnapshot document : task.getResult())
                            {
                                //Convert each item stored in the database to an object of the To Do class
                                ToDo element = document.toObject(ToDo.class);

                                //Manually assign the id that was provided by the database
                                element.setId(document.getId());

                                //Add the element to the To Do list
                                toDoList.add(element);

                                //Notify the adapter that some data has change, so he can update the corresponding view with the new info
                                adapter.notifyDataSetChanged();
                            }

                        }
                        else
                        {
                            //In case of errors, print the message.
                            Log.w("TODOList-get", "Error getting documents.", task.getException());
                        }


                    }
                });
    }
}
